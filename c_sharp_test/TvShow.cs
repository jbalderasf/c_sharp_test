﻿using System;
namespace c_sharp_test
{
    public class TvShow
    {
        private string name;
        private Int32 id;
        private Boolean isFavorite;
        public TvShow(string name, Int32 id, Boolean isFavorite)
        {
            this.name = name;
            this.id = id;
            this.isFavorite = isFavorite;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public Int32 Id
        {
            get { return id; }
            set { id = value; }
        }

        public Boolean IsFavorite
        {
            get { return isFavorite; }
            set { isFavorite = value; }
        }
    }
}
