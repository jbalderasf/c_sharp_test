﻿using System;
using System.Collections.Generic;

namespace c_sharp_test
{
    class Program
    {
        static List<TvShow> TvShowList = new List<TvShow>();
        static void Main(string[] args)
        {
            
            TvShowList.Add(new TvShow("Under the Dome", 1, false));
            TvShowList.Add(new TvShow("Person of Interest", 2, false));
            TvShowList.Add(new TvShow("Bitten", 3, true));
            TvShowList.Add(new TvShow("Arrow", 4, false));
            TvShowList.Add(new TvShow("True Detective", 5, false));

            while (true)
            { 
                var input = Console.ReadLine();
                if (input.Equals("exit", StringComparison.OrdinalIgnoreCase))
                {
                    break;
                }
                else if (input.Equals("favorites", StringComparison.OrdinalIgnoreCase))
                {
                    var myFavorites = TvShowList.FindAll(n => n.IsFavorite);
                    foreach (var item in myFavorites)
                    {
                        Console.WriteLine(item.Name + " " + item.Id);
                    }
                }
                else if (input.Equals("list", StringComparison.OrdinalIgnoreCase))
                {
                    TvShowList.Sort((n1, n2) => n1.Name.CompareTo(n2.Name));
                    foreach (var item in TvShowList)
                    {
                        if (item.IsFavorite)
                        {
                            Console.WriteLine(item.Name + " " + item.Id + "*");
                        }
                        else
                        {
                            Console.WriteLine(item.Name + " " + item.Id);
                        }
                    }
                }
                else
                {
                    try
                    {
                        int numVal = Int32.Parse(input);
                        var element = TvShowList.FindLast(n => n.Id == numVal);
                        element.IsFavorite = !element.IsFavorite;
                    }
                    catch (FormatException e)
                    {
                        Console.WriteLine("No index in the list");
                    }
                }
            }
        }
    }
}
